import matplotlib.pyplot as plt
import numpy as np

from ufl import ( TestFunction, ds, dx, dS, pi, sin, SpatialCoordinate,
                 TrialFunction, dot )
from dune.fem.space import dgonb
from dune.fem.function import gridFunction
#from dune.fem.scheme import molGalerkin as scheme
from dune.fem.scheme import galerkin as scheme
from dune.common import comm
from dune.grid import yaspGrid, cartesianDomain
from dune.alugrid import aluSimplexGrid

from dolfin_dg import (DGFemSIPG, DGFemNIPG, DGFemBO, DGDirichletBC,
                       PoissonOperator)

if comm.size > 1:
    NotImplementedError("Plotting in this demo will not work in parallel.")

run_count = 0
ele_ns = [4, 8, 16, 32, 64]
errorl2 = np.zeros(len(ele_ns))
errorh1 = np.zeros(len(ele_ns))
hsizes = np.zeros(len(ele_ns))
p = 2

# List of fluxes to be used in the comparison
fluxes = [("SIPG", DGFemSIPG),
          ("NIPG", DGFemNIPG),
          ("Baumann-Oden", DGFemBO)]

# Solve the linear Poisson problem and return the L2 and H1 errors, in addition
# to the mesh size.
def compute_error(ele_n, flux, dim = 2):
    # mesh = yaspGrid( cartesianDomain([0]*dim, [1]*dim, [ele_n]*dim ) )
    mesh = aluSimplexGrid( cartesianDomain([0]*dim, [1]*dim, [ele_n]*dim ) )

    V = dgonb(mesh, order=p)
    u = TrialFunction(V)
    v = TestFunction(V)
    x = SpatialCoordinate(V)

    gD = gridFunction(sin(pi*x[0])*sin(pi*x[1]), gridView=mesh, name="gD", order=p )
    f = gridFunction(2*pi*pi*sin(pi*x[0])*sin(pi*x[1]), gridView=mesh, name="gD", order=p)

    pe = PoissonOperator(mesh, V, DGDirichletBC(ds, gD))
    F = pe.generate_fem_formulation(u, v) - f*v*dx

    u_h = V.function(name="u_h")
    solver = scheme([F == 0], solver="gmres")
    solver.solve(target=u_h)

    errorl2 = integrate(dot(gD - u_h, gD - u_h ), order=2*p+1)
    # TODO
    # errorh1 = integrate(errornorm(gD, u_h, norm_type='h1', degree_rise=3)
    h_size = mesh.hmax()
    errorh1 = errorl2
    return (errorl2, errorh1, h_size)


# Compute rates of convergence
def compute_rate(errors, hsizes):
    return np.log(errors[0:-1]/errors[1:])/np.log(hsizes[0:-1]/hsizes[1:])


l2_plt = plt.figure(1).add_subplot(1, 1, 1)
h1_plt = plt.figure(2).add_subplot(1, 1, 1)
rate_messages = []
for name, flux in fluxes:
    error = np.array([compute_error(ele_n, flux) for ele_n in ele_ns])
    h_sizes = error[:, 2]

    l2_rates = compute_rate(error[:, 0], h_sizes)
    h1_rates = compute_rate(error[:, 1], h_sizes)

    rate_messages += [
        f"{name} flux\n"
        f"L2 rates: {str(l2_rates)}\n"
        f"H1 rates: {str(h1_rates)}"]

    l2_plt.loglog(h_sizes, error[:, 0], "-x")
    h1_plt.loglog(h_sizes, error[:, 1], "--x")


info("\n".join(rate_messages))

l2_plt.legend([name for (name, _) in fluxes])
l2_plt.set_ylabel("$\\Vert u - u_h \\Vert_{L_2}$")
l2_plt.set_xlabel("$h$")
l2_plt.set_title(f"$p = {p}$")

h1_plt.legend([name for (name, _) in fluxes])
h1_plt.set_ylabel("$\\Vert u - u_h \\Vert_{H_1}$")
h1_plt.set_xlabel("$h$")
h1_plt.set_title(f"$p = {p}$")

plt.show()
